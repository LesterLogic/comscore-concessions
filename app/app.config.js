'use strict';

//Configuration for the concessionApp module
//If the app needs more views or routing this is the place to add them
angular.module('concessionApp').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.
            when('/', {
                template: '<concession-stand></concession-stand>'
            }).
            otherwise('/');
    }
]);
