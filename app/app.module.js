'use strict';

//Define the 'concessionApp' module
angular.module('concessionApp', [
    'ngRoute',
    'cart',
    'concessionStand',
    'menu'
]);
