'use strict';

// Define the cart component
angular.module('cart').component('cart', {
    templateUrl: 'app/cart/cart.template.html',
    bindings: {
        'onRemoveCartItem': '&',
        'onCartCheckout': '&',
        'cartItems': '<'
    },
    controller: ['$scope', function CartController($scope) {
        var self = this;

        //Sum the totals of all cart items
        this.calculateCartTotal = function() {
            var total = 0;
            for (var item in self.cartItems) {
                total += self.cartItems[item].total;
            }

            return total;
        };
    }]
});