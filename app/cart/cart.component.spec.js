describe('component: cart', function() {
    var $componentController;

    beforeEach(module('concessionApp'));
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
        ctrl = $componentController('cart');
        ctrl.cartItems = { 'Popcorn': {"name": "Popcorn", "price": 3.00, "calc": "q*p", "quantity": 1, "total": 3.00 },
                           'Snickers': {"name": "Snickers", "price": 4.00, "calc": "(3*(~~(q/5))*p) + ((q%5)*p)", "quantity":1, "total": 4.00 }
                         };
    }));

    it('should return the correct cart total', function() {
        var total = ctrl.calculateCartTotal();
        expect(total).toEqual(7);
    });
});