'use strict';

// Define the concessionStand component
angular.module('concessionStand').component('concessionStand', {
    templateUrl: 'app/concession-stand/concession-stand.template.html',
    controller: ['$scope', function ConcessionStandController($scope) {
        var self = this;
        this.cartItems = {};

        //Add an item to the cart, adjust quantity, and calculate total
        this.addItemToCart = function(item) {
            if (self.cartItems[item['name']] === undefined) {
                self.cartItems[item['name']] = {
                    name: item['name'],
                    price: item['price'],
                    calc: item['calc'],
                    quantity: 1,
                    total: self.calculateLineTotal(item['price'], 1, item['calc'])
                };
            } else {
                var quantity = ++self.cartItems[item['name']]['quantity'];
                self.cartItems[item['name']] = {
                    name: item['name'],
                    price: item['price'],
                    calc: item['calc'],
                    quantity: quantity,
                    total: self.calculateLineTotal(item['price'], quantity, item['calc'])
                };
            }
        };

        //Remove all the items of a type from the cart
        this.removeItemFromCart = function(item) {
            if (self.cartItems[item['name']] !== undefined) {
                delete self.cartItems[item['name']];
            }
        };

        //Calculate the line total for an item
        //Swap the variables in the calc equation and solve
        //This allows for doing fancy price adjustments like the Snickers buy 5 for the price of 3
        this.calculateLineTotal = function(price, quantity, calc) {
            var calcString = calc;
            var priceRegex = /p/g;
            var quantityRegex = /q/g;
            calcString = calcString.replace(priceRegex, price);
            calcString = calcString.replace(quantityRegex, quantity);

            return eval(calcString);
        };

        //Remove all items from the cart
        this.clearCartItems = function() {
            this.cartItems = {};
        };
    }]
});
