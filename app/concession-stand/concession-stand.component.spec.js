describe('component: concessionStand', function() {
    var $componentController;

    beforeEach(module('concessionApp'));
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
        ctrl = $componentController('concessionStand');
    }));

    it('should add a new item to the cart', function() {
        var item = {"name": "Popcorn", "price": 3.00, "calc": "q*p"};
        ctrl.addItemToCart(item);

        expect(ctrl.cartItems['Popcorn']).toBeDefined();
        expect(ctrl.cartItems['Popcorn'].quantity).toEqual(1);
        expect(ctrl.cartItems['Popcorn'].total).toEqual(3);
    });

    it('should increase the quantity of an existing item in the cart', function() {
        var item = {"name": "Popcorn", "price": 3.00, "calc": "q*p"};
        ctrl.cartItems = { 'Popcorn': {"name": "Popcorn", "price": 3.00, "calc": "q*p", "quantity": 1, "total": 3.00 }};
        ctrl.addItemToCart(item);

        expect(ctrl.cartItems['Popcorn'].quantity).toEqual(2);
        expect(ctrl.cartItems['Popcorn'].total).toEqual(6);
    });

    it('should remove an item from the cart', function() {
        var item = {"name": "Popcorn", "price": 3.00, "calc": "q*p"};
        ctrl.cartItems = { 'Popcorn': {"name": "Popcorn", "price": 3.00, "calc": "q*p", "quantity": 1, "total": 3.00 },
                           'Snickers': {"name": "Snickers", "price": 4.00, "calc": "(3*(~~(q/5))*p) + ((q%5)*p)", "quantity":1, "total": 4.00 }
                         };
        ctrl.removeItemFromCart(item);

        expect(ctrl.cartItems['Popcorn']).not.toBeDefined();
        expect(ctrl.cartItems['Snickers']).toBeDefined();
    });

    it('should parse calc string and return correct value', function() {
        ctrl.cartItems = { 'Popcorn': {"name": "Popcorn", "price": 3.00, "calc": "q*p", "quantity": 3, "total": 3.00 },
                           'Snickers': {"name": "Snickers", "price": 4.00, "calc": "(3*(~~(q/5))*p) + ((q%5)*p)", "quantity":5, "total": 4.00 }
                         };
        var popcornTotal = ctrl.calculateLineTotal(ctrl.cartItems['Popcorn'].price,ctrl.cartItems['Popcorn'].quantity, ctrl.cartItems['Popcorn'].calc);
        var snickersTotal = ctrl.calculateLineTotal(ctrl.cartItems['Snickers'].price,ctrl.cartItems['Snickers'].quantity, ctrl.cartItems['Snickers'].calc);

        expect(popcornTotal).toEqual(9);
        expect(snickersTotal).toEqual(12);
    });

    it('should clear all items from the cart', function() {
        ctrl.cartItems = { 'Popcorn': {"name": "Popcorn", "price": 3.00, "calc": "q*p", "quantity": 3, "total": 3.00 },
                           'Snickers': {"name": "Snickers", "price": 4.00, "calc": "(3*(~~(q/5))*p) + ((q%5)*p)", "quantity":5, "total": 4.00 }
                         };
        ctrl.clearCartItems();

        expect(ctrl.cartItems).toEqual({});
    });
});
