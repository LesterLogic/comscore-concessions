'use strict';

// Define the menu component
angular.module('menu').component('menu', {
    templateUrl: 'app/menu/menu.template.html',
    bindings: {
        'onSelectMenuItem': '&'
    },
    controller: ['$scope', 'menuFactory', function MenuController($scope, menuFactory) {
        var self = this;
        this.menu = menuFactory.getNewMenu();

        this.$onInit = function() {
            self.menu.loadMenu();
        };
    }]
});