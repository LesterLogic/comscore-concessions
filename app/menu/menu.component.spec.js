describe('component: menu', function() {
    var $componentController;

    beforeEach(module('concessionApp'));
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
        ctrl = $componentController('menu');
    }));

    it('should be defined', function() {
        expect(ctrl).toBeDefined();
    });
});