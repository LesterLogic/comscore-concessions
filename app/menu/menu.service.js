'use strict';

//Define the menuFactory for creating new menus
angular.module('menu').
factory('menuFactory', ['$http', function($http) {
    //The menu prototype
    var Menu = function() {
        var self = this;
        this.items = [];

        //Reload the menu items from the json file
        //Menu can be reloaded in situ without reloading the entire app
        this.loadMenu = function() {
            $http.get('./app/menu/menu.json').then(function(response) {
                self.items = [];
                for (var x=0; x<response.data.length; x++) {
                    self.items.push(new MenuItem(response.data[x]));
                }
            });
        };
    };

    //The menu item prototype
    var MenuItem = function(menuItemData) {
        var self = this;
        this.name = menuItemData['name'];
        this.price = menuItemData['price'];
        this.calc = menuItemData['calc'];
    };

    return {
        getNewMenu: function() {
            return new Menu();
        }
    }
}]);