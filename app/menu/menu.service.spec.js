describe('service: menu', function() {
    var $httpBackend, authRequestHandler, menu;

    // Load the module that contains the 'menu' service before each test
    beforeEach(module('concessionApp'));

    beforeEach(inject(function(_menuFactory_) {
        factory = _menuFactory_;
        menu = factory.getNewMenu();
    }));

    beforeEach(inject(function($injector) {
        // Set up the mock http service responses
         $httpBackend = $injector.get('$httpBackend');
         // backend definition common for all tests
         authRequestHandler = $httpBackend.when('GET', './app/menu/menu.json')
            .respond([
                        {"name": "Popcorn", "price": 3.00, "calc": "q*p"},
                        {"name": "Snickers", "price": 4.00, "calc": "(3*(~~(q/5))*p) + ((q%5)*p)"},
                        {"name": "Soda", "price": 2.00, "calc": "q*p"}
                    ]);
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should load the items property with the menu items', function() {
        menu.loadMenu();
        $httpBackend.flush();
        expect(menu.items.length).toEqual(3);
    });
});