module.exports = function(config) {
    config.set({
        basePath: '.',

        files: [
            'node_modules/angular/angular.js',
            'node_modules/angular-route/angular-route.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'app/**/*.module.js',
            'app/**/*.service.js',
            'app/**/*.component.js',
            'app/**/*.spec.js'
        ],

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['PhantomJS'],

        plugins: [
            'karma-jasmine',
            'karma-phantomjs-launcher'
        ]
    });
};
